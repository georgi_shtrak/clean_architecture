package com.example.data.networking.details;

import com.example.data.MovieDetailsModel;

import rx.functions.Func1;

/**
 * Created by georginikolov on 5/29/18.
 */

public class DetailsMapper implements Func1<DetailsApiMovie, MovieDetailsModel> {
    @Override
    public MovieDetailsModel call(DetailsApiMovie detailsApiMovie) {
        return new MovieDetailsModel(detailsApiMovie.getId(),
                detailsApiMovie.getTitle(),
                detailsApiMovie.getYear(),
                detailsApiMovie.getRated(),
                detailsApiMovie.getReleased(),
                detailsApiMovie.getRuntime(),
                detailsApiMovie.getGenre(),
                detailsApiMovie.getLanguage(),
                detailsApiMovie.getPoster());
    }
}
