package com.example.data;

/**
 * Created by georginikolov on 5/22/18.
 */

public class Movie {

    private String id;
    private String title;
    private String imageUrl;

    public Movie(String title, String imageUrl, String id) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
