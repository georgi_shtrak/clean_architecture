package com.example.presentation.internal.di.components;


import com.example.presentation.ExampleApplication;
import com.example.presentation.Navigator;
import com.example.presentation.details.DetailsActivity;
import com.example.presentation.details.DetailsPresenter;
import com.example.presentation.input.InputActivity;
import com.example.presentation.input.InputPresenter;
import com.example.presentation.internal.di.modules.AppModule;
import com.example.presentation.internal.di.scopes.ApplicationScope;
import com.example.presentation.results.SearchResultsActivity;
import com.example.presentation.results.SearchResultsPresenter;

import dagger.Component;

/**
 * Created by georginikolov on 5/16/18.
 */
@ApplicationScope
@Component(dependencies = DataComponent.class, modules = {AppModule.class})
public interface AppComponent {

    void inject(ExampleApplication app);

    void inject(SearchResultsActivity searchResultsActivity);

    void inject(InputActivity inputActivity);

    void inject(DetailsActivity detailsActivity);

    SearchResultsPresenter provideSearchResultPresenter();

    DetailsPresenter provideDetailsPresenter();

    InputPresenter inputPresenter();

    Navigator provideNavigator();


}