package com.example.data.searchrepo;

import com.example.data.Movie;
import com.example.data.networking.search.MovieServiceContract;


import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public class SearchRepository implements MovieSearchRepository<Movie> {

    private MovieResultsMemoryCache<Movie> memoryCache;
    private MovieServiceContract service;
    private String search;

    @Inject
    public SearchRepository(MovieResultsMemoryCache<Movie> memoryCache, MovieServiceContract service) {
        this.memoryCache = memoryCache;
        this.service = service;
        this.search = "";
    }

    @Override
    public Observable<List<Movie>> getMoviesPage(String search, int page) {
        if (!this.search.equals(search)) {
            memoryCache.invalidate();
            this.search = search;
        }
        return Observable.concat(memory(search,page), network(search, page))
                .first(movie -> movie != null); //Try the memory cache, if empty try the server
    }

    private Observable<List<Movie>> memory(String search, int page){
        return memoryCache.get(search, page);
    }

    private Observable<List<Movie>> network(String search, int page){
        return service.getMovies(search, page).doOnNext(movies->{
            memoryCache.setCriteria(search);
            memoryCache.add(movies); //Save data to memory
        });
    }
}
