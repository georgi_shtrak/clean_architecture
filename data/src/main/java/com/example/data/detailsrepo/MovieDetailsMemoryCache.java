package com.example.data.detailsrepo;

import com.example.data.MovieDetailsModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public class MovieDetailsMemoryCache implements DetailsMemoryCache<MovieDetailsModel> {

    private List<MovieDetailsModel> data;

    @Inject
    public MovieDetailsMemoryCache(){
        this.data = new ArrayList<>();
    }

    @Override
    public void set(MovieDetailsModel movieDetailsModel) {
        this.data.add(movieDetailsModel);
    }

    @Override
    public void invalidate() {
        this.data.clear();
    }

    @Override
    public Observable<MovieDetailsModel> get(String id) {
        for(MovieDetailsModel movieDetailsModel : this.data){
            if(movieDetailsModel.getId().equals(id)){
                return Observable.just(movieDetailsModel);
            }
        }
        return Observable.just(null);
    }
}
