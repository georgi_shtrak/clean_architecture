package com.example.rx.delivery;

import rx.Notification;
import rx.Observable;
import rx.functions.Func2;

public class RxDeliveryDelegate<R> {

    private Observable<R> targetObservable;
    private Observable<Boolean> targetStatusObservable;

    public RxDeliveryDelegate(Observable<R> targetObservable, Observable<Boolean> targetStatusObservable) {
        if(targetObservable == null){
            throw new IllegalArgumentException("Null target Observable provided.");
        }

        if(targetStatusObservable == null){
            throw new IllegalArgumentException("Null status Observable provided.");
        }

        this.targetObservable = targetObservable;
        this.targetStatusObservable = targetStatusObservable;
    }

    /**
     * Returns a transformer that will delay onNext, onError and onComplete emissions unless a view become available.
     * getView() is guaranteed to be != null during all emissions. This transformer can only be used on application's main thread.
     * <p/>
     * Use this operator if you need to deliver *all* emissions to a view, in example when you're sending items
     * into adapter one by one.
     *
     * @param <T> a type of onNext value.
     * @return the delaying operator.
     */
    public  <T> Observable.Transformer<T, Delivery<R,T>> deliver(){
        return observable -> observable.lift(
                OperatorSemaphore.semaphore(targetStatusObservable))
                .materialize()
                .filter(notification -> !notification.isOnCompleted())
                .withLatestFrom(targetObservable, (Func2<Notification<T>, R, Delivery<R, T>>) (notification, view) -> {
                    return new Delivery<R, T>(view, notification);
                });
    }

    /**
     * Returns a transformer that will delay onNext, onError and onComplete emissions unless a view become available.
     * getView() is guaranteed to be != null during all emissions. This transformer can only be used on application's main thread.
     * <p/>
     * If this transformer receives a next value while the previous value has not been delivered, the
     * previous value will be dropped.
     * <p/>
     * Use this operator when you need to show updatable data.
     *
     * @param <T> a type of onNext value.
     * @return the delaying operator.
     */
    public <T> Observable.Transformer<T, Delivery<R,T>> deliverLatest(){
        return observable -> observable.lift(
                OperatorSemaphore.semaphoreLatest(targetStatusObservable))
                .materialize()
                .filter(notification -> !notification.isOnCompleted())
                .withLatestFrom(targetObservable, (Func2<Notification<T>, R, Delivery<R, T>>) (notification, view) -> {
                    return new Delivery<R, T>(view, notification);
                });
    }
}
