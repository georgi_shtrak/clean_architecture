package com.example.data.networking.search;

import com.google.gson.annotations.SerializedName;

/**
 * Created by georginikolov on 5/29/18.
 */

public class ApiMovie implements MovieModel{

    @SerializedName("imdbID")
    private String id;
    @SerializedName("Title")
    private String title;
    @SerializedName("Year")
    private String year;
    @SerializedName("Type")
    private String type;
    @SerializedName("Poster")
    private String imageUrl;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getType() {
        return type;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
