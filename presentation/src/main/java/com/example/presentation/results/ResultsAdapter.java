package com.example.presentation.results;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.data.Movie;
import com.example.presentation.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by georginikolov on 5/23/18.
 */

public class ResultsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_DATA = 2;
    private static final int VIEW_TYPE_LOADING = 3;
    private boolean isLoadingAdded = false;
    private OnItemClickListener onItemClickListener;

    private List<Movie> data;

    ResultsAdapter(List<Movie> data, OnItemClickListener onItemClickListener){
        this.data = data;
        this.onItemClickListener = onItemClickListener;
    }

    boolean isEmpty(){
        return data == null || data.isEmpty();
    }

    boolean isLoading() {
        return isLoadingAdded;
    }

    void hideLoading(){
        isLoadingAdded = false;
        data.remove(data.size() - 1);
        notifyItemRemoved(data.size());
    }

    void appendData(List<Movie> newData){
        isLoadingAdded = false;
        data.remove(data.size() - 1);
        int currentCount = getItemCount();
        this.data.addAll(newData);
        notifyItemRangeChanged(currentCount - 1, currentCount + newData.size());
    }

    void showLoading(){
        isLoadingAdded = true;
        data.add(new Movie("", "", ""));
        notifyItemRangeChanged(data.size() - 1, 1);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_DATA){
            return new ResultsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.result_cell, parent, false));
        }else{
            return new LoadingViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_cell, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1 && isLoadingAdded) ? VIEW_TYPE_LOADING : VIEW_TYPE_DATA;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == VIEW_TYPE_DATA){
            ResultsViewHolder resultsViewHolder = (ResultsViewHolder) holder;
            Movie movie = data.get(position);
            resultsViewHolder.title.setText(movie.getTitle());
            Glide.with(holder.itemView.getContext())
                    .load(movie.getImageUrl())
                    .apply(new RequestOptions()
                            .placeholder(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.cat)))
                    .into(resultsViewHolder.poster);
        }
    }

    class ResultsViewHolder extends RecyclerView.ViewHolder {

        ImageView poster;
        TextView title;

        ResultsViewHolder(View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.movie_image);
            title = itemView.findViewById(R.id.movie_title);
            itemView.setOnClickListener(view -> {
                if(ResultsAdapter.this.onItemClickListener != null){
                    ResultsAdapter.this.onItemClickListener.onItemClicked(data.get(getAdapterPosition()));
                }
            });
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {

        LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(Movie movie);
    }
}
