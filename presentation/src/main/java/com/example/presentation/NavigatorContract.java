package com.example.presentation;

/**
 * Created by georginikolov on 5/31/18.
 */

public interface NavigatorContract {

    void goToSearchResultsScreen(String query);

    void goToDetailsActivity(String id);
}
