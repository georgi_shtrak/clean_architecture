package com.example.presentation;

import android.app.Application;

import com.example.presentation.internal.di.components.AppComponent;
import com.example.presentation.internal.di.components.DaggerAppComponent;
import com.example.presentation.internal.di.components.DaggerDataComponent;
import com.example.presentation.internal.di.modules.AppModule;


/**
 * Created by georginikolov on 5/16/18.
 */

public class ExampleApplication extends Application {

    static AppComponent appComponent;

        @Override
        public void onCreate() {
            super.onCreate();
            appComponent = DaggerAppComponent.builder()
                    .dataComponent(DaggerDataComponent.create())
                    .appModule(new AppModule(this))
                    .build();
            appComponent.inject(this);

        }

       public static AppComponent getComponent(){
            return appComponent;
        }
}
