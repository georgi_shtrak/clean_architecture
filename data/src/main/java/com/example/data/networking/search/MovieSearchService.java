package com.example.data.networking.search;


import com.example.data.Movie;
import com.example.data.networking.base.BaseService;
import com.example.data.networking.base.RetrofitInterface;
import com.example.data.searchrepo.ApiMovieToMovieMapper;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */
@RetrofitInterface(retrofitApi = MovieApi.class)
public class MovieSearchService extends BaseService<MovieApi> implements MovieServiceContract{

    @Inject
    public MovieSearchService(Retrofit retrofit) {
        super(retrofit);
    }

    @Override
    public Observable<List<Movie>> getMovies(String search, int page){
        return serviceApi.getMovies(apiKey, search, String.valueOf(page))
                .flatMap(apiSearchResult -> Observable.from(apiSearchResult.getMovieList())
                        .map(apiMovie -> new ApiMovieToMovieMapper().call(apiMovie))
                        .toList());
    }
}
