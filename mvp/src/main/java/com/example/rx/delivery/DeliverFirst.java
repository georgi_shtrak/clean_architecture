package com.example.rx.delivery;

import rx.Observable;

public class DeliverFirst<View, T> implements Observable.Transformer<T, Delivery<View, T>> {

    private final Observable<View> view;

    public DeliverFirst(Observable<View> view) {
        this.view = view;
    }

    @Override
    public Observable<Delivery<View, T>> call(Observable<T> observable) {
        return Observable
                .combineLatest(
                        view,
                        observable.materialize().take(1),
                        (view, notification) -> view == null ? null : new Delivery<>(view, notification))
                .filter(delivery -> delivery != null)
                .take(1);
    }
}

