package com.example.presentation.details;

import com.example.data.MovieDetailsModel;
import com.example.presentation.base.BaseView;
import com.example.presentation.base.LoadingView;

/**
 * Created by georginikolov on 5/23/18.
 */

public interface DetailsView extends LoadingView {

    void displayData(MovieDetailsModel model);
}
