package com.example.data.networking.search;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public interface MovieApi {

    @GET("/")
    Observable<ApiSearchResult> getMovies(@Query(value="apiKey", encoded=true) String apiKey, @Query(value = "s", encoded = true) String search, @Query(value = "page", encoded = true) String pageNumber);
}
