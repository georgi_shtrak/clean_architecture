package com.example.rx.delivery;

import rx.Notification;
import rx.Observable;
import rx.Subscription;
import rx.subjects.ReplaySubject;

public class DeliverReplay<View, T> implements Observable.Transformer<T, Delivery<View, T>> {

    private final Observable<View> view;

    public DeliverReplay(Observable<View> view) {
        this.view = view;
    }

    @Override
    public Observable<Delivery<View, T>> call(Observable<T> observable) {
        final ReplaySubject<Notification<T>> subject = ReplaySubject.create();
        final Subscription subscription = observable
                .materialize()
                .filter(notification -> !notification.isOnCompleted())
                .subscribe(subject);
        return view
                .switchMap(view -> view == null ? Observable.never() : subject
                        .map(notification -> new Delivery<>(view, notification)))
                .doOnUnsubscribe(subscription::unsubscribe);
    }
}