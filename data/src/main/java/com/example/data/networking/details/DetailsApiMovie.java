package com.example.data.networking.details;

import com.google.gson.annotations.SerializedName;

/**
 * Created by georginikolov on 5/29/18.
 */

public class DetailsApiMovie {

    @SerializedName("imdbID")
    private String id;
    @SerializedName("Title")
    private String title;
    @SerializedName("Year")
    private String year;
    @SerializedName("Rated")
    private String rated;
    @SerializedName("Released")
    private String released;
    @SerializedName("Runtime")
    private String runtime;
    @SerializedName("Genre")
    private String genre;
    @SerializedName("Language")
    private String language;
    @SerializedName("Poster")
    private String poster;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getRated() {
        return rated;
    }

    public String getReleased() {
        return released;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getGenre() {
        return genre;
    }

    public String getLanguage() {
        return language;
    }

    public String getPoster() {
        return poster;
    }
}
