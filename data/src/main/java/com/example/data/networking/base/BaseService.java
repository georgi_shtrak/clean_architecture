package com.example.data.networking.base;

/**
 * Created by xComputers on 05/08/2017.
 */

import retrofit2.Retrofit;

/**
 * A base implementation of a service which all service classes should extend
 * @param <Api> the Retrofit interface to make the server calls
 */

public abstract class BaseService<Api>{

    protected Api serviceApi;
    private Retrofit retrofit;
    protected String apiKey = "8b50d17c";

    protected BaseService(Retrofit retrofit) {
        this.retrofit = retrofit;
        serviceApi = provideServiceApi();
    }

    @SuppressWarnings("unchecked")
    protected Api provideServiceApi() {

        if (getClass().getAnnotation(RetrofitInterface.class) == null) {
            throw new RuntimeException(String.format("You don't have an retrofitApi annotation on %s", this.getClass().getCanonicalName()));
        }
        return retrofit.create((Class<Api>) getClass().getAnnotation(RetrofitInterface.class).retrofitApi());
    }
}