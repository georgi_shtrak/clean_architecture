package com.example.presentation.details;

import com.example.domain.details.GetDetailMovieUseCase;
import com.example.presentation.ErrorMessageFactory;
import com.example.presentation.Navigator;
import com.example.presentation.base.LoadingView;
import com.example.rx.RxPresenter;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by georginikolov on 5/23/18.
 */

public class DetailsPresenter extends RxPresenter<DetailsView> {

    private GetDetailMovieUseCase movieUseCase;

    @Inject
    public DetailsPresenter(GetDetailMovieUseCase movieUseCase) {
        this.movieUseCase = movieUseCase;
    }

    void getMovie(String movieId) {
        doWhenViewBound(LoadingView::showLoading);
        this.add(movieUseCase.execute(movieId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(deliver())
                .subscribe(delivery -> delivery.split(
                        (view, result) -> {
                            view.hideLoading();
                            view.displayData(result);
                        }, (view, throwable) -> {
                            view.hideLoading();
                            view.displayError(ErrorMessageFactory.create(throwable));
                        })
                )
        );
    }
}
