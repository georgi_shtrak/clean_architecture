package com.example.domain.results;

import com.example.data.Movie;
import com.example.data.searchrepo.MovieSearchRepository;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by georginikolov on 5/22/18.
 */

public class GetMoviesUseCase implements GetMoviesUseCaseContract<List<Movie>> {

    private MovieSearchRepository<Movie> repository;

    @Inject
    public GetMoviesUseCase(MovieSearchRepository<Movie> repository) {
        this.repository = repository;
    }

    public Observable<List<Movie>> execute(String queryString, int page) {
        return repository.getMoviesPage(queryString, page);
    }
}
