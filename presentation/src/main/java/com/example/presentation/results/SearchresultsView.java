package com.example.presentation.results;

import com.example.data.Movie;
import com.example.presentation.base.LoadingView;

import java.util.List;

/**
 * Created by georginikolov on 5/22/18.
 */

public interface SearchresultsView extends LoadingView {

    void displayMovies(List<Movie> movies);

    void showAdapterLoading();
}
