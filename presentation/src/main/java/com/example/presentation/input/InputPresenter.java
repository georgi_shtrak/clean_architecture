package com.example.presentation.input;

import android.text.TextUtils;

import com.example.presentation.Navigator;
import com.example.presentation.NavigatorContract;
import com.example.rx.RxPresenter;

import javax.inject.Inject;

/**
 * Created by georginikolov on 5/22/18.
 */

public class InputPresenter extends RxPresenter<InputView> {

    private NavigatorContract navigator;

    @Inject
    public InputPresenter(NavigatorContract navigator){
        this.navigator = navigator;
    }


    void searchClicked(String movieSearch){
        if(TextUtils.isEmpty(movieSearch)){
            doWhenViewBound(inputView -> inputView.displayError("Please provide a non empty input."));
            return;
        }
        navigator.goToSearchResultsScreen(movieSearch);
    }
}
