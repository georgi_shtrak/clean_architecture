package com.example.presentation.results;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.activities.AppCompatViewActivity;
import com.example.data.Movie;
import com.example.presentation.AlertDialogFactory;
import com.example.presentation.ExampleApplication;
import com.example.presentation.R;
import com.example.presentation.RecyclerViewScrollListener;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Created by georginikolov on 5/22/18.
 */

public class SearchResultsActivity extends AppCompatViewActivity<SearchResultsPresenter> implements SearchresultsView {

    public static final String MOVIES_SEARCH_KEY = "SearchResultsActivity.MOVIES_SEARCH_KEY";

    @Inject
    Provider<SearchResultsPresenter> provider;

    private RecyclerView moviesRecycler;
    private ResultsAdapter adapter;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movies_list);

        ExampleApplication.getComponent().inject(this);
        String queryString = getIntent().getStringExtra(MOVIES_SEARCH_KEY);

        moviesRecycler = findViewById(R.id.movies_recycler);
        progressBar = findViewById(R.id.movie_list_progress_bar);

        getPresenter().getMovies(queryString);
    }


    @Override
    public void displayError(String errorMessage) {
        AlertDialogFactory.create(this, errorMessage, (dialog, which) -> {
            //TODO maybe move this to a delegate
            if(adapter == null || adapter.isEmpty()){
                getPresenter().getMovies(getIntent().getStringExtra(MOVIES_SEARCH_KEY));
            }else{
                dialog.dismiss();
                adapter.hideLoading();
            }
        }).show();
    }

    @Override
    public void displayMovies(List<Movie> movies) {
        if (adapter == null) {
            createAdapter(movies);
        } else {
            adapter.appendData(movies);
        }
    }

    @Override
    public void showAdapterLoading() {
        adapter.showLoading();
    }

    private void createAdapter(List<Movie> movies) {
        adapter = new ResultsAdapter(movies, movie -> getPresenter().onItemClicked(movie));
        moviesRecycler.setAdapter(adapter);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        moviesRecycler.setLayoutManager(manager);
        setupScrollListener();
    }

    private void setupScrollListener() {
        moviesRecycler.addOnScrollListener(new RecyclerViewScrollListener(() -> {
            if (!adapter.isLoading()) {
                getPresenter().getNextPage();
            }
        }));
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        moviesRecycler.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        moviesRecycler.setVisibility(View.VISIBLE);
    }

    @Override
    public SearchResultsPresenter createPresenter() {
        return provider.get();
    }
}
