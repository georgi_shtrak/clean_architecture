package com.example.presentation.results;

import com.example.data.Movie;
import com.example.domain.results.GetMoviesUseCaseContract;
import com.example.presentation.ErrorMessageFactory;
import com.example.presentation.NavigatorContract;
import com.example.presentation.base.LoadingView;
import com.example.rx.RxPresenter;

import java.util.List;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by georginikolov on 5/22/18.
 */

public class SearchResultsPresenter extends RxPresenter<SearchresultsView> {

    private GetMoviesUseCaseContract<List<Movie>> moviesUseCase;
    private NavigatorContract navigator;

    private String queryString;
    private int page;

    @Inject
    public SearchResultsPresenter(GetMoviesUseCaseContract<List<Movie>> getMoviesUseCase, NavigatorContract navigator){
        this.moviesUseCase = getMoviesUseCase;
        this.navigator = navigator;
        queryString = "";
        page = 1;
    }


    void getMovies(String queryString){
        doWhenViewBound(LoadingView::showLoading);
        page = 1;
        this.queryString = queryString;
        getMoviesByPage(queryString, page);
    }

    void onItemClicked(Movie movie){
        navigator.goToDetailsActivity(movie.getId());
    }

    private void getMoviesByPage(String queryString, int page){
        if(page > 1){
            doWhenViewBound(SearchresultsView::showAdapterLoading);
        }
        this.add(moviesUseCase.execute(queryString, page)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .compose(deliver())
        .subscribe(delivery -> delivery.split((searchResultsView, movies) -> {
            searchResultsView.hideLoading();
            searchResultsView.displayMovies(movies);
        }, (searchresultsView, throwable) -> {
            searchresultsView.hideLoading();
            searchresultsView.displayError(ErrorMessageFactory.create(throwable));
        })));
    }

    void getNextPage(){
        getMoviesByPage(queryString, ++page);
    }

}
