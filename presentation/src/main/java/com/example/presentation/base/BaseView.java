package com.example.presentation.base;

/**
 * Created by georginikolov on 5/22/18.
 */

public interface BaseView {

    void displayError(String errorMessage);
}
