package com.example.support_fragments_v7;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;

import com.example.core.PresenterStorage;


@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
public class FragmentPresenterStorage {

    private FragmentPresenterStorage() {
    }

    public static PresenterStorage from(FragmentManager fragmentManager){
        if (fragmentManager == null) {
            throw new IllegalArgumentException("FragmentManager argument cannot be null.");
        }

        PresenterStorageFragment fragment = (PresenterStorageFragment)
                fragmentManager.findFragmentByTag(PresenterStorageFragment.TAG);
        if (fragment == null) {
            fragment = new PresenterStorageFragment();
            fragmentManager.beginTransaction()
                    .add(fragment, PresenterStorageFragment.TAG)
                    .disallowAddToBackStack()
                    .commitAllowingStateLoss();
        }
        return fragment.getPresenterStorage();
    }
}
