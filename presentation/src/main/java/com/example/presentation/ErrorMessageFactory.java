package com.example.presentation;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by georginikolov on 5/23/18.
 */

public class ErrorMessageFactory {

    public static String create(Throwable throwable){
        if(throwable instanceof UnknownHostException){
            return "Unable to connect to server.";
        }else if(throwable instanceof SocketTimeoutException){
            return "Request took too long.";
        }else{
            return "Something went wrong";
        }
    }
}
