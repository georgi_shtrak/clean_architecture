package com.example.domain.results;

import rx.Observable;

/**
 * Created by georginikolov on 5/31/18.
 */

public interface GetMoviesUseCaseContract<T> {

    Observable<T> execute(String queryString, int page);
}
