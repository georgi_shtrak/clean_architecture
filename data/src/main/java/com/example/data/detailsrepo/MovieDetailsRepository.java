package com.example.data.detailsrepo;

import com.example.data.MovieDetailsModel;
import com.example.data.networking.details.DetailsMapper;
import com.example.data.networking.details.DetailsServiceContract;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public class MovieDetailsRepository implements DetailsRepo<MovieDetailsModel> {

    private DetailsMemoryCache<MovieDetailsModel> memoryCache;
    private DetailsServiceContract service;

    @Inject
    public MovieDetailsRepository(DetailsMemoryCache<MovieDetailsModel> memoryCache, DetailsServiceContract service) {
        this.memoryCache = memoryCache;
        this.service = service;
    }

    @Override
    public Observable<MovieDetailsModel> getMovie(String id) {
        return Observable.concat(memoryCache.get(id),
                service.getMovie(id)
                        .map(detailsApiMovie -> {
                            MovieDetailsModel movieDetailsModel = new DetailsMapper().call(detailsApiMovie);
                            memoryCache.set(movieDetailsModel);
                            return movieDetailsModel;
                        }))
                .first(movie -> movie != null);
    }
}
