package com.example.domain.details;

import com.example.data.MovieDetailsModel;
import com.example.data.detailsrepo.DetailsRepo;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by georginikolov on 5/23/18.
 */

public class GetDetailMovieUseCase {

    private DetailsRepo<MovieDetailsModel> repository;

    @Inject
    public GetDetailMovieUseCase(DetailsRepo<MovieDetailsModel> repository){
        this.repository = repository;
    }

    public Observable<MovieDetailsModel> execute(String id){
        return repository.getMovie(id);
    }
}
