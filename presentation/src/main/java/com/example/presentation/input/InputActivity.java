package com.example.presentation.input;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.EditText;

import com.example.activities.AppCompatViewActivity;
import com.example.presentation.AlertDialogFactory;
import com.example.presentation.ExampleApplication;
import com.example.presentation.R;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Created by georginikolov on 5/22/18.
 */

public class InputActivity extends AppCompatViewActivity<InputPresenter> implements InputView {

    @Inject
    Provider<InputPresenter> provider;

    private EditText inputET;
    private Button searchBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input);
        ExampleApplication.getComponent().inject(this);
        initViews();
        searchBtn.setOnClickListener(v -> getPresenter().searchClicked(inputET.getText().toString()));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyViews();
    }

    @Override
    public InputPresenter createPresenter() {
        return provider.get();
    }

    private void initViews() {
        inputET = findViewById(R.id.input_et);
        searchBtn = findViewById(R.id.search_button);
    }

    private void destroyViews() {
        inputET = null;
        searchBtn = null;
    }

    @Override
    public void displayError(String errorMessage) {
        AlertDialogFactory.create(this, errorMessage, (dialog, which) -> dialog.dismiss()).show();
    }
}
