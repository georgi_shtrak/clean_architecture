package com.example.data.networking.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by georginikolov on 5/29/18.
 */

public class ApiSearchResult {

    @SerializedName("Search")
    private List<ApiMovie> movieList;

    public List<ApiMovie> getMovieList() {
        return movieList;
    }
}
