package com.example.presentation.base;

/**
 * Created by georginikolov on 5/22/18.
 */

public interface LoadingView extends BaseView {

    void showLoading();
    void hideLoading();
}
