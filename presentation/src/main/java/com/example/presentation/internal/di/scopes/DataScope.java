package com.example.presentation.internal.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by georginikolov on 5/17/18.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface DataScope {
}