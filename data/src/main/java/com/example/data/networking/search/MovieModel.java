package com.example.data.networking.search;

/**
 * Created by georginikolov on 5/29/18.
 */

public interface MovieModel {

    String getId();

    String getTitle();

    String getYear();

    String getType();

   String getImageUrl();
}
