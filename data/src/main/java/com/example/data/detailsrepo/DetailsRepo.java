package com.example.data.detailsrepo;


import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public interface DetailsRepo<Т> {

     Observable<Т> getMovie(String id);
}
