package com.example.data.networking.details;

import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public interface DetailsServiceContract {

    Observable<DetailsApiMovie> getMovie(String id);
}
