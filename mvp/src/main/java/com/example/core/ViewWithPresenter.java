package com.example.core;

public interface ViewWithPresenter<P extends Presenter> {

    /**
     * @return the currently attached presenter or null.
     */
    P getPresenter();
}
