package com.example.presentation.internal.di.modules;

import android.content.Context;


import com.example.data.Movie;
import com.example.data.MovieDetailsModel;
import com.example.data.detailsrepo.DetailsMemoryCache;
import com.example.data.detailsrepo.DetailsRepo;
import com.example.data.detailsrepo.MovieDetailsMemoryCache;
import com.example.data.detailsrepo.MovieDetailsRepository;
import com.example.data.networking.details.DetailsService;
import com.example.data.networking.details.DetailsServiceContract;
import com.example.data.networking.search.MovieSearchService;
import com.example.data.networking.search.MovieServiceContract;
import com.example.data.searchrepo.MovieMemoryCache;
import com.example.data.searchrepo.MovieResultsMemoryCache;
import com.example.data.searchrepo.MovieSearchRepository;
import com.example.data.searchrepo.SearchRepository;
import com.example.domain.results.GetMoviesUseCase;
import com.example.domain.results.GetMoviesUseCaseContract;
import com.example.presentation.Navigator;
import com.example.presentation.NavigatorContract;
import com.example.presentation.internal.di.scopes.ApplicationScope;


import java.util.List;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by georginikolov on 5/16/18.
 */
@Module
public class AppModule {

    Context context;

    public AppModule(Context context){
        this.context = context;
    }

    @Provides
    @ApplicationScope
    Context provideContext() {
        return context;
    }

    @Provides
    NavigatorContract provideNavigator(Context context){
        return new Navigator(context);
    }

    @Provides
    MovieResultsMemoryCache<Movie> provideMemoryCache(){
        return new MovieMemoryCache();
    }

    @Provides
    MovieSearchRepository<Movie> provideRepository(MovieResultsMemoryCache<Movie> memoryCache, MovieServiceContract service){
        return new SearchRepository(memoryCache, service);
    }

    @Provides
    MovieServiceContract provideService(Retrofit retrofit){
        return new MovieSearchService(retrofit);
    }

    @Provides
    DetailsServiceContract provideServiceContract(Retrofit retrofit){
        return new DetailsService(retrofit);
    }

    @Provides
    DetailsRepo<MovieDetailsModel> provideMovieDetailsRepo(MovieDetailsMemoryCache memoryCache, DetailsService serviceContract){
        return new MovieDetailsRepository(memoryCache, serviceContract);
    }

    @Provides
    DetailsMemoryCache<MovieDetailsModel> provideDetailsMemoryCache(){
        return new MovieDetailsMemoryCache();
    }

    @Provides
    GetMoviesUseCaseContract<List<Movie>> provideGetMoviesUseCase(MovieSearchRepository<Movie> repository){
        return new GetMoviesUseCase(repository);
    }
}
