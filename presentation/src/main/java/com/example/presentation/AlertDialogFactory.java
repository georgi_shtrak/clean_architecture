package com.example.presentation;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by georginikolov on 5/22/18.
 */

public class AlertDialogFactory {

    public static AlertDialog create(Context context, String errorMessage, DialogInterface.OnClickListener onClickAction){
        return new AlertDialog.Builder(context)
                .setPositiveButton("OK", onClickAction)
                .setTitle("Error")
                .setMessage(errorMessage)
                .create();
    }
}
