package com.example.presentation;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by georginikolov on 5/29/18.
 */

public class RecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private OnBottomReachedCallback callback;

    public RecyclerViewScrollListener(OnBottomReachedCallback callback){
        this.callback = callback;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
        int totalItemCount = recyclerView.getLayoutManager().getItemCount();
        int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                && firstVisibleItemPosition >= 0) {
            callback.onBottomReached();
        }
    }

    public interface OnBottomReachedCallback{
        void onBottomReached();
    }
}
