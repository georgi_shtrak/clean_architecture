package com.example.data.searchrepo;

import java.util.List;

import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public interface MovieResultsMemoryCache<T> {

    void add(List<T> list);

    void invalidate();

    Observable<List<T>> get(String search, int page);

    void setCriteria(String searchCriteria);
}
