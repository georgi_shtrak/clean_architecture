package com.example.data.searchrepo;

import com.example.data.Movie;
import com.example.data.networking.search.MovieModel;

import rx.functions.Func1;

/**
 * Created by georginikolov on 5/29/18.
 */

public class ApiMovieToMovieMapper implements Func1<MovieModel, Movie> {
    @Override
    public Movie call(MovieModel apiMovie) {
        return new Movie(apiMovie.getTitle(), apiMovie.getImageUrl(), apiMovie.getId());
    }
}
