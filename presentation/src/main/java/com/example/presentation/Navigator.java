package com.example.presentation;

import android.content.Context;
import android.content.Intent;

import com.example.presentation.details.DetailsActivity;
import com.example.presentation.internal.di.scopes.ApplicationScope;
import com.example.presentation.results.SearchResultsActivity;

import javax.inject.Inject;

/**
 * Created by georginikolov on 5/22/18.
 */
@ApplicationScope
public class Navigator implements NavigatorContract{

    private Context context;

    @Inject
    public Navigator(Context context){
        this.context = context;
    }

    public void goToSearchResultsScreen(String query){
        //The analytics can go here
        context.startActivity(new Intent(context, SearchResultsActivity.class)
                .putExtra(SearchResultsActivity.MOVIES_SEARCH_KEY, query)
        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void goToDetailsActivity(String id){
        context.startActivity(new Intent(context, DetailsActivity.class)
                .putExtra(DetailsActivity.MOVIE_ID_KEY, id)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}
