package com.example.data.searchrepo;

import java.util.List;

import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public interface MovieSearchRepository<T> {

    Observable<List<T>> getMoviesPage(String search, int page);
}
