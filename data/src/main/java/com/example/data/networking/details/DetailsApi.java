package com.example.data.networking.details;

import com.example.data.networking.search.ApiSearchResult;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public interface DetailsApi {

    @GET("/")
    Observable<DetailsApiMovie> getMovieDetails(@Query(value="apiKey", encoded=true) String apiKey, @Query(value = "i", encoded = true) String search);
}
