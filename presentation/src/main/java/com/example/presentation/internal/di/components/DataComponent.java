package com.example.presentation.internal.di.components;

import com.example.data.networking.details.DetailsService;
import com.example.data.networking.search.MovieSearchService;
import com.example.presentation.internal.di.scopes.DataScope;
import com.example.presentation.internal.di.modules.RetrofitModule;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by georginikolov on 5/17/18.
 */
@DataScope
@Component(modules = RetrofitModule.class)
public interface DataComponent {

    Retrofit provideRetrofit();
    MovieSearchService provideExampleService();
    DetailsService detailsService();

}
