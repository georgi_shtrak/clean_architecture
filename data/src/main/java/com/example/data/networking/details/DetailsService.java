package com.example.data.networking.details;

import com.example.data.networking.base.BaseService;
import com.example.data.networking.base.RetrofitInterface;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */
@RetrofitInterface(retrofitApi = DetailsApi.class)
public class DetailsService extends BaseService<DetailsApi> implements DetailsServiceContract {

    @Inject
    public DetailsService(Retrofit retrofit) {
        super(retrofit);
    }

    @Override
    public Observable<DetailsApiMovie> getMovie(String id) {
        return serviceApi.getMovieDetails(apiKey, id);
    }
}
