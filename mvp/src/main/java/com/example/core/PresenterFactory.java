package com.example.core;

public interface PresenterFactory<P extends Presenter> {
    P createPresenter();
}