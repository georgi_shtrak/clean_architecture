package com.example.data.networking.search;

import com.example.data.Movie;

import java.util.List;

import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public interface MovieServiceContract {

    Observable<List<Movie>> getMovies(String search, int page);
}
