package com.example.data.searchrepo;

import com.example.data.Movie;
import com.example.data.networking.search.MovieModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by georginikolov on 5/17/18.
 */

public class MovieMemoryCache implements MovieResultsMemoryCache<Movie> {

    private List<Movie> data;
    private String search;

    @Inject
    public MovieMemoryCache() {
        data = new ArrayList<>();
        this.search = "";
    }

    public void setCriteria(String searchCriteria) {
        this.search = searchCriteria;
    }

    @Override
    public void add(List<Movie> list) {
        this.data.addAll(list);
    }

    public void invalidate() {
        this.search = "";
        this.data.clear();
    }

    //Hardcoded 10 items per page for simplycity
    //in reality this information will be returned from the server and these calculations will be dynamic
    public Observable<List<Movie>> get(String search, int page) {
        if (!this.search.equals(search)) {
            return Observable.just(null);
        }
        int startingIndex = ((page - 1) * 10) + 1;
        int endIndex = (startingIndex + 10) >= data.size() ? data.size() - 1 : startingIndex + 9;
        if (this.data.size() >= startingIndex) {
            return Observable.from(data.subList(startingIndex - 1, endIndex)).toList();
        }
        return Observable.just(null);
    }
}
