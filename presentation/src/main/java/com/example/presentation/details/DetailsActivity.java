package com.example.presentation.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.activities.AppCompatViewActivity;
import com.example.data.MovieDetailsModel;
import com.example.presentation.AlertDialogFactory;
import com.example.presentation.ExampleApplication;
import com.example.presentation.R;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Created by georginikolov on 5/23/18.
 */

public class DetailsActivity extends AppCompatViewActivity<DetailsPresenter> implements DetailsView {

    public static final String MOVIE_ID_KEY = "DetailsActivity.MOVIE_ID_KEY";

    private TextView title;
    private TextView year;
    private TextView rated;
    private TextView released;
    private TextView runtime;
    private TextView genre;
    private TextView language;
    private ImageView poster;

    @Inject
    Provider<DetailsPresenter> provider;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);
        initViews();
        ExampleApplication.getComponent().inject(this);
        String movieId = getIntent().getStringExtra(MOVIE_ID_KEY);
        getPresenter().getMovie(movieId);
    }

    @Override
    public DetailsPresenter createPresenter() {
        return provider.get();
    }

    @Override
    public void displayError(String errorMessage) {
        AlertDialogFactory.create(this, errorMessage, (dialogInterface, which) -> {
            dialogInterface.dismiss();
            onBackPressed();
        });
    }

    @Override
    public void showLoading() {
        //TODO implementation
    }

    @Override
    public void hideLoading() {
        //TODO implementation
    }

    @Override
    public void displayData(MovieDetailsModel model) {
        Glide.with(this)
                .load(model.getPoster())
                .apply(new RequestOptions()
                        .placeholder(ContextCompat.getDrawable(this, R.drawable.cat)))
                .into(poster);
        title.setText(model.getTitle());
        year.setText(model.getYear());
        rated.setText(model.getRated());
        released.setText(model.getReleased());
        runtime.setText(model.getRuntime());
        genre.setText(model.getGenre());
        language.setText(model.getLanguage());
    }

    private void initViews() {

        poster = findViewById(R.id.poster);
        title = findViewById(R.id.title);
        year = findViewById(R.id.year);
        rated = findViewById(R.id.rated);
        released = findViewById(R.id.released);
        runtime = findViewById(R.id.runtime);
        genre = findViewById(R.id.genre);
        language = findViewById(R.id.language);
    }

    private void destroyViews() {

        poster = null;
        title = null;
        year = null;
        rated = null;
        released = null;
        runtime = null;
        genre = null;
        language = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyViews();
    }
}
