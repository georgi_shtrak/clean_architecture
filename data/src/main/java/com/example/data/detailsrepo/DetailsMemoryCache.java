package com.example.data.detailsrepo;

import rx.Observable;

/**
 * Created by georginikolov on 5/29/18.
 */

public interface DetailsMemoryCache<T> {

    void set(T t);

    void invalidate();

    Observable<T> get(String id);
}
